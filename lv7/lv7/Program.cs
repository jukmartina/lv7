﻿/*Napišite sučelje IPayable koje ima metode koje omogućuju dohvaćanje
iznosa, dodavanje na iznos i umanjenje iznosa za određeni iznos koji se prima
kao parametar. Kreirajte dvije klase koje predstavljaju bankovni račun
(podatkovni članovi za iznos, maksimalni minus i ime) i mobilni račun (pod.
članovi za broj, stanje i cijenu poruke) te implementiraju dano sučelje.
Kreirajte listu čiji su elementi objekti danih klasa, umetnite po dva objekta
dane klase i testirajte sučelje ispisom na konzolu. Potrebno je uplatiti
nasumičan iznos (između 10 i 100), podići nasumičan iznos (između 1 i 10) te
ispisati trenutno stanje.*/


using System;
using System.Collections.Generic;

namespace lv7
{
    class Program
    {
        static void Main(string[] args)
        {
            Random rand = new Random();
            Banka randomBank = new Banka(65,"Martina", -450);
            Banka randBank = new Banka(34, "Magdalena", -67);
            Mobitel randomMob = new Mobitel("+38763548349", 78, 1.6);
            Mobitel randMob = new Mobitel("+436434646", 35, 0.16);
        // randomPlane.fly();
        // randomBird.fly();
            List<IPayable> racun = new List<IPayable>();
            racun.Add(randomBank);
            racun.Add(randBank);
            racun.Add(randomMob);
            racun.Add(randMob);
            foreach (IPayable pay in racun)
            {
                Console.WriteLine( "Iznos na racunu:" +pay.get());
            }
            randomBank.isplati(rand.NextDouble()*10);
            randomMob.uplati(rand.NextDouble()*90+10);
            randBank.uplati(rand.NextDouble() * 90+10);
            randMob.isplati(rand.NextDouble() * 10);
            foreach (IPayable pay in racun)
            {
                Console.WriteLine("Iznos na racunu:" + pay.get());
            }
        }
    }
    class Banka : IPayable
    {
        double iznos;
        string ime;
        double maxMinus;
        public Banka(float iznos, string name, float max)
        {
            this.iznos = iznos;
            this.ime = name;
            this.maxMinus = max;
        }
        public double get()
        {
            return iznos;
        }

        public void isplati(double x)
        {
            if (iznos - x >= maxMinus) iznos -= x;
            else Console.WriteLine("Nedovoljan iznos na bankovnom racunu!");
        }

        public void uplati(double x)
        {
            iznos += x;
        }
    }
    class Mobitel : IPayable
    {
        string broj;
        double stanje;
        double cPoruka;

        public Mobitel(string broj, double stanje, double cPoruka)
        {
            this.broj = broj;
            this.stanje = stanje;
            this.cPoruka = cPoruka;
        }

        public double get()
        {
            return stanje;
        }

        public void isplati(double x)
        {
            if (stanje - x>= 0) stanje -= x;
            else Console.WriteLine("Nedovoljan iznos na mobilnom racunu!");
        }

        public void uplati(double x)
        {
            stanje += x;
        }
    }
    interface IPayable
    {
        double get();
        void uplati(double x);
        void isplati(double x);
    }
}


