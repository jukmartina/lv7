﻿namespace lv7z2
{
    partial class Form1
    {
        /// <summary>
        /// Erforderliche Designervariable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Verwendete Ressourcen bereinigen.
        /// </summary>
        /// <param name="disposing">True, wenn verwaltete Ressourcen gelöscht werden sollen; andernfalls False.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Vom Windows Form-Designer generierter Code

        /// <summary>
        /// Erforderliche Methode für die Designerunterstützung.
        /// Der Inhalt der Methode darf nicht mit dem Code-Editor geändert werden.
        /// </summary>
        private void InitializeComponent()
        {
            this.slika = new System.Windows.Forms.PictureBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.krug = new System.Windows.Forms.RadioButton();
            this.kvadar = new System.Windows.Forms.RadioButton();
            this.elipsa = new System.Windows.Forms.RadioButton();
            this.crvena = new System.Windows.Forms.RadioButton();
            this.zelena = new System.Windows.Forms.RadioButton();
            this.plava = new System.Windows.Forms.RadioButton();
            this.rbShape = new System.Windows.Forms.GroupBox();
            this.rbColor = new System.Windows.Forms.GroupBox();
            this.hScrollBar1 = new System.Windows.Forms.HScrollBar();
            this.debljina = new System.Windows.Forms.Label();
            this.Close = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.slika)).BeginInit();
            this.rbShape.SuspendLayout();
            this.rbColor.SuspendLayout();
            this.SuspendLayout();
            // 
            // slika
            // 
            this.slika.Location = new System.Drawing.Point(12, 12);
            this.slika.Name = "slika";
            this.slika.Size = new System.Drawing.Size(506, 426);
            this.slika.TabIndex = 0;
            this.slika.TabStop = false;
            this.slika.MouseUp += new System.Windows.Forms.MouseEventHandler(this.slika_MouseUp);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(556, 18);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 13);
            this.label1.TabIndex = 1;
            this.label1.Text = "Shape:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(556, 179);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(34, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Color:";
            // 
            // krug
            // 
            this.krug.AutoSize = true;
            this.krug.Location = new System.Drawing.Point(16, 7);
            this.krug.Name = "krug";
            this.krug.Size = new System.Drawing.Size(51, 17);
            this.krug.TabIndex = 3;
            this.krug.TabStop = true;
            this.krug.Text = "Circle";
            this.krug.UseVisualStyleBackColor = true;
            // 
            // kvadar
            // 
            this.kvadar.AutoSize = true;
            this.kvadar.Location = new System.Drawing.Point(16, 46);
            this.kvadar.Name = "kvadar";
            this.kvadar.Size = new System.Drawing.Size(59, 17);
            this.kvadar.TabIndex = 4;
            this.kvadar.TabStop = true;
            this.kvadar.Text = "Square";
            this.kvadar.UseVisualStyleBackColor = true;
            // 
            // elipsa
            // 
            this.elipsa.AutoSize = true;
            this.elipsa.Location = new System.Drawing.Point(16, 90);
            this.elipsa.Name = "elipsa";
            this.elipsa.Size = new System.Drawing.Size(53, 17);
            this.elipsa.TabIndex = 5;
            this.elipsa.TabStop = true;
            this.elipsa.Text = "Elipse";
            this.elipsa.UseVisualStyleBackColor = true;
            // 
            // crvena
            // 
            this.crvena.AutoSize = true;
            this.crvena.Location = new System.Drawing.Point(9, 7);
            this.crvena.Name = "crvena";
            this.crvena.Size = new System.Drawing.Size(45, 17);
            this.crvena.TabIndex = 6;
            this.crvena.TabStop = true;
            this.crvena.Text = "Red";
            this.crvena.UseVisualStyleBackColor = true;
            // 
            // zelena
            // 
            this.zelena.AutoSize = true;
            this.zelena.Location = new System.Drawing.Point(9, 45);
            this.zelena.Name = "zelena";
            this.zelena.Size = new System.Drawing.Size(54, 17);
            this.zelena.TabIndex = 7;
            this.zelena.TabStop = true;
            this.zelena.Text = "Green";
            this.zelena.UseVisualStyleBackColor = true;
            // 
            // plava
            // 
            this.plava.AutoSize = true;
            this.plava.Location = new System.Drawing.Point(9, 85);
            this.plava.Name = "plava";
            this.plava.Size = new System.Drawing.Size(46, 17);
            this.plava.TabIndex = 8;
            this.plava.TabStop = true;
            this.plava.Text = "Blue";
            this.plava.UseVisualStyleBackColor = true;
            // 
            // rbShape
            // 
            this.rbShape.Controls.Add(this.elipsa);
            this.rbShape.Controls.Add(this.kvadar);
            this.rbShape.Controls.Add(this.krug);
            this.rbShape.Location = new System.Drawing.Point(559, 47);
            this.rbShape.Name = "rbShape";
            this.rbShape.Size = new System.Drawing.Size(131, 120);
            this.rbShape.TabIndex = 9;
            this.rbShape.TabStop = false;
            // 
            // rbColor
            // 
            this.rbColor.Controls.Add(this.plava);
            this.rbColor.Controls.Add(this.zelena);
            this.rbColor.Controls.Add(this.crvena);
            this.rbColor.Location = new System.Drawing.Point(555, 205);
            this.rbColor.Name = "rbColor";
            this.rbColor.Size = new System.Drawing.Size(134, 116);
            this.rbColor.TabIndex = 10;
            this.rbColor.TabStop = false;
            // 
            // hScrollBar1
            // 
            this.hScrollBar1.Location = new System.Drawing.Point(564, 381);
            this.hScrollBar1.Name = "hScrollBar1";
            this.hScrollBar1.Size = new System.Drawing.Size(126, 17);
            this.hScrollBar1.TabIndex = 11;
            this.hScrollBar1.Scroll += new System.Windows.Forms.ScrollEventHandler(this.hScrollBar1_Scroll);
            // 
            // debljina
            // 
            this.debljina.AutoSize = true;
            this.debljina.Location = new System.Drawing.Point(556, 342);
            this.debljina.Name = "debljina";
            this.debljina.Size = new System.Drawing.Size(56, 13);
            this.debljina.TabIndex = 12;
            this.debljina.Text = "Thickness";
            // 
            // Close
            // 
            this.Close.Location = new System.Drawing.Point(713, 415);
            this.Close.Name = "Close";
            this.Close.Size = new System.Drawing.Size(75, 23);
            this.Close.TabIndex = 13;
            this.Close.Text = "Close";
            this.Close.UseVisualStyleBackColor = true;
            this.Close.Click += new System.EventHandler(this.Close_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.Close);
            this.Controls.Add(this.debljina);
            this.Controls.Add(this.hScrollBar1);
            this.Controls.Add(this.rbColor);
            this.Controls.Add(this.rbShape);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.slika);
            this.Name = "Form1";
            this.Text = "Form1";
            ((System.ComponentModel.ISupportInitialize)(this.slika)).EndInit();
            this.rbShape.ResumeLayout(false);
            this.rbShape.PerformLayout();
            this.rbColor.ResumeLayout(false);
            this.rbColor.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox slika;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.RadioButton krug;
        private System.Windows.Forms.RadioButton kvadar;
        private System.Windows.Forms.RadioButton elipsa;
        private System.Windows.Forms.RadioButton crvena;
        private System.Windows.Forms.RadioButton zelena;
        private System.Windows.Forms.RadioButton plava;
        private System.Windows.Forms.GroupBox rbShape;
        private System.Windows.Forms.GroupBox rbColor;
        private System.Windows.Forms.HScrollBar hScrollBar1;
        private System.Windows.Forms.Label debljina;
        private System.Windows.Forms.Button Close;
    }
}

