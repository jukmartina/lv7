﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace lv7z2
{
    public partial class Form1 : Form
    {
        Graphics g;
        Circle c = new Circle();
        Square s = new Square();
        Elipse el = new Elipse();
        int deblj;
        Pen pen = new Pen(Color.Black);
        public Form1()
        {
            InitializeComponent();
            g = slika.CreateGraphics();
        }

        private void hScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            deblj = hScrollBar1.Value;
        }

        private void slika_MouseUp(object sender, MouseEventArgs e)
        {
            if (crvena.Checked) pen = new Pen(Color.Red, deblj);
            else if (zelena.Checked) pen = new Pen(Color.Green, deblj);
            else if (plava.Checked) pen = new Pen(Color.Blue, deblj);
            else MessageBox.Show("Nije odabrana boja.");

                if (krug.Checked)
                {
                c.draw(g, pen, e.X, e.Y);
                }
                else if ( kvadar.Checked)
                {
                    s.draw(g, pen, e.X, e.Y);
                }
                else if ( elipsa.Checked)
                {
                    el.draw(g, pen, e.X, e.Y);
                }
                else MessageBox.Show("Niste unjeli oblik!");
            
        }

        private void Close_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }

    class Circle:IDrawable
    {
        int r;
        public Circle() { r = 10; }
        public void draw(Graphics g, Pen p, int x, int y)
        {
            g.DrawEllipse(p, x, y, r, r);
        }
    }

    class Square: IDrawable
    {
        int a;
        public Square() { a = 10; }
        public void draw(Graphics g, Pen p, int x, int y)
        {
            g.DrawRectangle(p, x, y, a, a);
        }
    }

    class Elipse: IDrawable    {
        int a, b;
        public Elipse() { a = 10; b = 15;  }
        public void draw(Graphics g, Pen p, int x, int y)
        {
            g.DrawEllipse(p, x, y, a, b);
        }
    }
}

interface IDrawable
{
    void draw(Graphics g, Pen p, int x, int y);
}
